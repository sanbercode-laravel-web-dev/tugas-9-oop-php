<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>
<body>
    <h2>PHP Object Oriented Programming</h2>
    <h4>Release 0</h4>
    <?php
        $sheep = new Animal("Shaun");
        echo "Name : " . $sheep->name ."<br>";
        echo "Legs : " . $sheep->legs ."<br>";
        echo "Cold Blooded : " . $sheep->cold_blooded ."<br>";
    ?>
    <h4>Realese 1</h4>
    <?php 
        $kodok = new Frog("Buduk");
        echo "Name : " . $kodok->name ."<br>";
        echo "Legs : " . $kodok->legs ."<br>";
        echo "Cold Blooded  : " .$kodok->cold_blooded ."<br>";
        echo "Jump : " .$kodok->jump ."<br>";
    ?>
    <br>
    <?php 
        $sungokong = new Ape("Kera Sakti");
        echo "Name : " . $sungokong->name ."<br>";
        echo "Legs : " . $sungokong->legs ."<br>";
        echo "Cold Blooded  : " .$sungokong->cold_blooded ."<br>";
        echo "Yell : " .$sungokong->yell ."<br>";
    ?>
</body>
</html>